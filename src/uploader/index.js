'use strict';
const request = require('request');
const path = require('path');
const fs = require('fs');
const URL = require('url');

class Uploader {
  constructor(url) {
    let info = URL.parse(url);
    let host = info.host;
    this.url = url;

    this.request = request.defaults({
      headers: {
        'Accept': '*/*',
        'Accept-Encoding': 'gzip, deflate',
        'Accept-Language': 'zh-CN,zh;q=0.8,en;q=0.6',
        'Host': host,
        'Origin': `http://${host}`,
        'Referer': `http://${host}/`,
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36',
        'X-Requested-With': 'ShockwaveFlash/24.0.0.186',
      },
    });
  }

  upload(file) {
    return new Promise((resolve, reject) => {
      if (!file.match(/\.txt$/i)) {
        resolve();
      }
      console.log('processing', file);
      let basename = path.basename(file);
      this.request({
        url: this.url,
        method: 'post',
        formData: {
          Filename: basename,
          Filedata: {
            value: fs.createReadStream(file),
            options: {
              filename: basename,
              contentType: 'application/octet-stream',
            },
          },
          Upload: 'Submit Query',
        },
      }, (err, res, body) => {
        err ? reject(err) : resolve(body);
      });
    });
  }
}

module.exports = Uploader;
