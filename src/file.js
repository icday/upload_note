'use strict';

const co = require('co');
const fs = require('fs');
const path = require('path');

function readdir(dir) {
  return co(function *() {
    return yield fs.readdir.bind(fs, dir);
  });
}

function stat(path) {
  return co(function *() {
    return yield fs.stat.bind(fs, path);
  });
}

function getFileTree(dir, maxLevel, level) {
  if (maxLevel === undefined) {
    maxLevel = 1000;
  }
  level = level || 0;
  return co(function *() {
    let st = yield stat(dir);
    if (!st.isDirectory()) {
      return dir;
    }
    if (level >= maxLevel) {
      return undefined;
    }

    let files = yield readdir(dir);
    let promises = files.map(function *(file) {
      file = path.resolve(dir, file);
      return yield getFileTree(file, maxLevel, level + 1);
    });

    let info = {};
    files.forEach((file, i) => {
      info[file] = promises[i];
    });
    return yield info;
  });
}

function getFileList(dir) {
  return co(function *() {
    let files = yield readdir(dir);
    console.log(files);
  });
}

exports.getFileList = getFileList;
exports.getFileTree = getFileTree;
