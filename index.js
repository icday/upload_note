'use strict';
const Uploader = require('./src/uploader');
const File = require('./src/file');
const co = require('co');

let host = process.argv[2];
let dir = process.argv[3];
if (!host || !dir) {
  console.log('Invalid args');
  process.exit(1);
}

function processTree(uploader, tree) {
  return co(function *() {
    switch(typeof tree) {
      case 'string':
        yield uploader.upload(tree);
        break;
      case 'object':
        let obj = {};
        for (let key in tree) {
          yield processTree(uploader, tree[key]);
        }
        break;
      default:
        return;
    }
  });
}

co(function *() {
  let uploader = new Uploader(`http://${host.replace('http://', '')}`);
  let fileTree = yield File.getFileTree(dir);

  yield processTree(uploader, fileTree);
}).then(res => {
  console.log('done');
}, err => {
  console.log('error', err);
});
